import{_ as s,c as n,o as a,a as e}from"./app.4dbe3a77.js";const m=JSON.parse('{"title":"","description":"","frontmatter":{},"headers":[{"level":2,"title":"\u5B89\u88C5\u4E0E\u4F7F\u7528","slug":"\u5B89\u88C5\u4E0E\u4F7F\u7528","link":"#\u5B89\u88C5\u4E0E\u4F7F\u7528","children":[]}],"relativePath":"dist/README.md"}'),p={name:"dist/README.md"},l=e(`<p>\u6570\u636E\u53EF\u89C6\u5316\u5E73\u53F0 / <a href="./modules.html">Modules</a></p><h2 id="\u5B89\u88C5\u4E0E\u4F7F\u7528" tabindex="-1">\u5B89\u88C5\u4E0E\u4F7F\u7528 <a class="header-anchor" href="#\u5B89\u88C5\u4E0E\u4F7F\u7528" aria-hidden="true">#</a></h2><div class="language-bash"><button title="Copy Code" class="copy"></button><span class="lang">bash</span><pre><code><span class="line"><span style="color:#676E95;"># \u521D\u59CB\u5316\u9879\u76EE</span></span>
<span class="line"><span style="color:#A6ACCD;">pnpm run bootstrap</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;"># \u8DD1\u8D77\u6765\uFF01</span></span>
<span class="line"><span style="color:#A6ACCD;">pnpm run dev</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;"># \u6784\u5EFA\u53D1\u5E03</span></span>
<span class="line"><span style="color:#A6ACCD;">pnpm run build</span></span>
<span class="line"></span>
<span class="line"><span style="color:#676E95;"># \u672C\u5730\u9884\u89C8\uFF0C\u9700\u8981\u5148\u6267\u884C build</span></span>
<span class="line"><span style="color:#A6ACCD;">pnpm run serve</span></span>
<span class="line"></span></code></pre></div>`,3),t=[l];function o(c,r,i,d,_,u){return a(),n("div",null,t)}const A=s(p,[["render",o]]);export{m as __pageData,A as default};
